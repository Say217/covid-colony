﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Feet : MonoBehaviour
{
    PlayerControl playerControl;

    private void Start() {
        playerControl = GetComponentInParent<PlayerControl>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {   
        // Kena Moving Platform
        if(collision.gameObject.CompareTag("Moving Platform")){
            // Debug.Log("masuk moving");
            playerControl.setOnPlatform(true);
            playerControl.transform.parent = collision.gameObject.transform;
        }

        if(!playerControl.getJump()){
            setPlayerJump(collision);
        }

    }

    private void OnTriggerStay2D(Collider2D collision) {
        if(!playerControl.getJump()){
            setPlayerJump(collision);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        // Keluar Moving Platform 
        if(collision.gameObject.CompareTag("Moving Platform")){
            // Debug.Log("keluar moving");
            playerControl.transform.parent = null;
        }
    }

    private void setPlayerJump(Collider2D collision){
        bool isFeetOnGround = collision.gameObject.CompareTag("Moving Platform")
        || collision.gameObject.CompareTag("environment")
        || collision.gameObject.CompareTag("obstacle");

        if(collision.gameObject.CompareTag("obstacle") || collision.gameObject.CompareTag("environment")){
            playerControl.setOnPlatform(false);
            if(collision.gameObject.name == "Platform"){
                playerControl.setOnPlatform(true);
            }
        }
        // Debug.Log(isFeetOnGround);
        // Kalo di ground dan gak gerak vertikal
        if (isFeetOnGround && playerControl.GetComponent<Rigidbody2D>().velocity.y == 0)
  		{
            // Debug.Log("unity answers saves the day!");
            // OnLandEvent.Invoke();
            playerControl.setJump();
        }
    }
}
