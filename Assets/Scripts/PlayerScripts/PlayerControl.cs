﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour
{
    public CharacterController2D controller;

    // Game Manager Vars
    private GameManager gm;

    // Movement Vars
    public float runSpeed;
    public float fallTimer;
    bool canMove = true;
    bool canJump = true;

    float horizontalMove = 0f;
    bool jump = false;

    // Component Vars
    private Rigidbody2D rb;

    // Barrel Vars
    [SerializeField]
    private GameObject barrel;
    private Transform barrelTransform;
    [SerializeField]
    private Transform barrelTip;

    // Bullet Vars
    public GameObject[] bullets; // Index 0 = normal, 1 = Pierce
    public float fireRate;
    public float bulletSpeed;
    private int bulletIndex = 0;
    private float fireCD;
    private bool isPierceReady = true;
    private float pierceCD;
    public float pierceCooldown = 5f;
    public Image pierceBWImage;
    public GameObject pierceHoverImage;

    // Direction Vars
    private Vector2 mouseDirection;
    private float lookAngle;

    // Platform Check
    [SerializeField] private bool onPlatform = false;

    // Masker
    public PlayerHPController HPC;
    private float maskerCD;
    public float maskerRate;
    public Image maskerBWImage;
    public GameObject respawnParticle;

    // Animator
    public Animator bodyAnim;
    public Animator handAnim;
    public Animator legsAnim;
    public Animator maskAnim;
    bool isFacingRight = true;
    public GameObject bodyPart;
    
    // Win
    GameObject winMenu;

    // Start is called before the first frame update
    void Start()
    {   
        rb = GetComponent<Rigidbody2D>();
        barrelTransform = barrel.GetComponent<Transform>();
        fireCD = fireRate;
        pierceCD = 0f;
        pierceBWImage.fillAmount = 0f;
        maskerBWImage.fillAmount = 0f;
        maskerCD = maskerRate;
        gm = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameManager>();
        transform.position = gm.lastCheckpointPos;

        // Spawn Animation
        Quaternion respawnRot = Quaternion.identity;
        respawnRot.eulerAngles = new Vector3(-90, 0, 0);
        Vector3 respawnPos = transform.position;
        respawnPos = respawnPos - new Vector3(0, 0.8f, 0);
        GameObject respawn = Instantiate(respawnParticle, respawnPos, respawnRot) as GameObject;
        respawn.transform.parent = this.transform;
    }

    // Update is called once per frame
    void Update()
    {
        // No Update if cannot move
        // Add other disabling bools
        if(!canMove || QuizMenu.isShown || PauseMenu.GameIsPaused || TutorialTrigger.isShown){
            bodyPart.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
            bodyAnim.SetFloat("speed", 0);
            handAnim.SetFloat("speed", 0);
            legsAnim.SetFloat("speed", 0);
            bodyAnim.SetBool("isShooting", false);
            handAnim.SetBool("isShooting", false);
            legsAnim.SetBool("isShooting", false);
            legsAnim.SetBool("isJumping", false);
            bodyAnim.SetBool("isJumping", false);
            handAnim.SetBool("isJumping", false);
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, gameObject.GetComponent<Rigidbody2D>().velocity.y);
            return;
        }

        // Move Keys
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

        bodyAnim.SetFloat("speed", Mathf.Abs(horizontalMove));
        handAnim.SetFloat("speed", Mathf.Abs(horizontalMove));
        legsAnim.SetFloat("speed", Mathf.Abs(horizontalMove));

        if(isFacingRight && horizontalMove < 0){
            Flip();
        } else if(!isFacingRight && horizontalMove > 0){
            Flip();
        }

        if (canJump && (Input.GetButtonDown("Jump") || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W)))
        {
            jump = true;
            canJump = false;

            bodyAnim.SetBool("isJumping", true);
            handAnim.SetBool("isJumping", true);
            legsAnim.SetBool("isJumping", true);
        }
        if (canJump && Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (controller.getIsGrounded() && onPlatform && gameObject.GetComponent<Rigidbody2D>().velocity.y == 0){
                offPlatform();
                canJump = false;
            }
        }
        // Pierce Key
        if (isPierceReady && Input.GetKeyDown(KeyCode.Alpha4)){
            bulletIndex = 1;

            // Activate Hover HUD
            if(pierceHoverImage.activeSelf){
                pierceHoverImage.SetActive(false);
                bulletIndex = 0; // reset to normal bullet, TODO: prev bullet
            }else{
                pierceHoverImage.SetActive(true);
            }
        }

        // Get Mouse/Cursor Position
        mouseDirection = Camera.main.ScreenToWorldPoint(Input.mousePosition) - barrelTransform.position;
        lookAngle = Mathf.Atan2(mouseDirection.y, mouseDirection.x) * Mathf.Rad2Deg;

        if (fireCD >= fireRate)
        {
            bodyPart.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
            bodyAnim.SetBool("isShooting", false);
            handAnim.SetBool("isShooting", false);
            legsAnim.SetBool("isShooting", false);
        }
        // Fire
        if (Input.GetKey(KeyCode.Mouse0))
        {
            // Flip character

            if(isFacingRight && mouseDirection.x < 0){
                Flip();
                // barrelTip.position = new Vector3(-1.57f, 0, 0);
            } else if(!isFacingRight && mouseDirection.x >0){
                Flip();
                // barrelTip.position = new Vector3(1.57f, 0, 0);
            }

            if(isFacingRight){
                bodyPart.transform.rotation = Quaternion.Euler(0f, 0f, lookAngle);
                barrelTip.rotation = Quaternion.Euler(0f, 0f, lookAngle);
            }else{
                bodyPart.transform.rotation = Quaternion.Euler(0f, 0f, lookAngle + 180);
                barrelTip.rotation = Quaternion.Euler(0f, 0f, lookAngle);
            }

            // Normal Bullet
            if(bulletIndex == 0 && fireCD >= fireRate){
                bodyAnim.SetBool("isShooting", true);
                handAnim.SetBool("isShooting", true);
                legsAnim.SetBool("isShooting", true);
                Fire();
                fireCD = 0;
            }

            // Pierce Cooldown longer (pasti udah ready)
            if (bulletIndex == 1)
            {
                bodyAnim.SetBool("isShooting", true);
                handAnim.SetBool("isShooting", true);
                legsAnim.SetBool("isShooting", true);
                Fire();
                fireCD = -0.2f;
                pierceHoverImage.SetActive(false);
                bulletIndex = 0; // reset to normal bullet, TODO: prev bullet
                isPierceReady = false;
                pierceBWImage.fillAmount = 1f;
                pierceCD = pierceCooldown; // CD khusus pierce bullet
            }
        }

        // Fire cooldown
        if (fireCD < fireRate)
        {
            fireCD += Time.deltaTime;
        }

        // Pierce cooldown
        if (pierceCD > 0){
            pierceCD -= Time.deltaTime;
            pierceBWImage.fillAmount -= 1 / pierceCooldown * Time.deltaTime;
        } else if(pierceCD <= 0 && !isPierceReady) {
            isPierceReady = true;
        }
        
        // Masker
        if (Input.GetKey(KeyCode.F) && (maskerCD >= maskerRate))
        {
            maskAnim.SetTrigger("isMaskOn");
            HPC.MaskOn();
            maskerCD = 0;
            maskerBWImage.fillAmount = 1f;
        }

        // Masker cooldown
        if (maskerCD < maskerRate)
        {
            maskerCD += Time.deltaTime;
            maskerBWImage.fillAmount -= 1 / maskerRate * Time.deltaTime;
        }
    }

    void FixedUpdate()
    {
        // No Update if cannot move
        if(!canMove){
            return;
        }

        // Move our Character
        controller.Move(horizontalMove * Time.fixedDeltaTime, false, jump);
        jump = false;
    }

    public void setCanMove(bool newMove){
        canMove = newMove;

        // Kalo gaboleh gerak, di 0 in
        if(!newMove){
            horizontalMove = 0f;
            controller.Move(horizontalMove, false, jump);
        }
    }

    public bool getCanMove(){
        return canMove;
    }

    public void setJump(){
        legsAnim.SetBool("isJumping", false);
        bodyAnim.SetBool("isJumping", false);
        handAnim.SetBool("isJumping", false);
        canJump = true;
    }

    public bool getJump(){
        return canJump;
    }

    public void setOnPlatform(bool newOnPlatform){
        onPlatform = newOnPlatform;
    }

    public bool getOnPlatform(){
        return onPlatform;
    }

    private void offPlatform()
    {
        GetComponent<CapsuleCollider2D>().isTrigger = true;
        // GetComponent<BoxCollider2D>().isTrigger = true;
    }

    void Fire()
    {
        // Debug.Log("Fire");
        GameObject bulletInstance = Instantiate(bullets[bulletIndex], barrelTip.position, barrelTip.rotation);
        bulletInstance.GetComponent<Rigidbody2D>().AddForce(bulletInstance.GetComponent<Transform>().right * bulletSpeed);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {   
        // Turun Tilemap Platform atau Turun moving platform
        if (collision.gameObject.name == "Platform" || collision.gameObject.CompareTag("Moving Platform")){
            // Debug.Log("keluar trigger");
            GetComponent<CapsuleCollider2D>().isTrigger = false;
            // GetComponent<BoxCollider2D>().isTrigger = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {   
        // Kena Tilemap Platform
        if (collision.gameObject.name == "Platform"){
            onPlatform = true;
            // Debug.Log("masuk Platform");
        }
        // Kena Tilemap Non Platform
        else if (collision.gameObject.name == "Non Platform"){
            onPlatform = false;
            // Debug.Log("masuk Non Platform");
        } else if(collision.gameObject.CompareTag("Moving Platform")){
            // Debug.Log("box trigger nyala");
            // GetComponent<BoxCollider2D>().isTrigger = true;
        } else if(collision.gameObject.CompareTag("obstacle")){
            setOnPlatform(false);
        }

    }

    private void OnCollisionExit2D(Collision2D collision) {
        if(collision.gameObject.CompareTag("Moving Platform")){
            // GetComponent<BoxCollider2D>().isTrigger = false;
        }
    }

    private void Flip()
	{
		// Switch the way the player is labelled as facing.
		isFacingRight = !isFacingRight;

		// Multiply the player's x local scale by -1.
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
}
