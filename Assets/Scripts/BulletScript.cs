﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public float speed;
    public float damage;
    public string bulletType;
    private Rigidbody2D rb;
    // Start is called before the first frame update

    // Animations
    public GameObject enemyHitAnim;

    void Start()
    {    }

    private void Update()
    {
    }

    // Use for Pierce Bullet only
    private void OnTriggerEnter2D(Collider2D collision) {
        // Debug.Log(collision.gameObject.name);
        // Damage enemies with crit chance
        if (collision.gameObject.tag == "enemy"){
            collision.gameObject.GetComponent<HPController>().TakeDamage(damage, GameManager.critChance, bulletType);
            if(bulletType != "pierce"){
                Destroy(gameObject);
            }

            // Player Hit Anim
            Quaternion hitRot = Quaternion.identity;
            hitRot.eulerAngles = new Vector3(0f, 0f, 0f);
            Vector3 hitPos = transform.position;
            GameObject hitAnimObject = Instantiate(enemyHitAnim, hitPos, hitRot) as GameObject;
        }
        else if (collision.gameObject.CompareTag("Flu"))
        {
            collision.gameObject.GetComponent<FluHPController>().TakeDamage(damage, GameManager.critChance, bulletType);
            Destroy(gameObject);
            Quaternion hitRot = Quaternion.identity;
            hitRot.eulerAngles = new Vector3(0f, 0f, 0f);
            Vector3 hitPos = transform.position;
            GameObject hitAnimObject = Instantiate(enemyHitAnim, hitPos, hitRot) as GameObject;
        }
        // Damage obstacles with no crit chance
        else if (collision.gameObject.tag == "obstacle"){
            // Debug.Log("pierce hit obstacle");
            collision.gameObject.GetComponent<HPController>().TakeDamage(damage, -1f, bulletType);
            Destroy(gameObject);
        } 
        // Others Destroy
        else if (collision.gameObject.CompareTag("environment") || collision.gameObject.CompareTag("Moving Platform")){
            // Debug.Log("bullet hit environment");
            Destroy(gameObject);
        }
    }

}
