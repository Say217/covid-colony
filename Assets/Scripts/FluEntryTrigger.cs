﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FluEntryTrigger : MonoBehaviour
{
    public GameObject bossCamera;
    private void OnTriggerEnter2D(Collider2D collision) {
        if(collision.gameObject.tag == "ThePlayer"){
            Quaternion oriRot = Quaternion.identity;
            Vector3 oriPos = bossCamera.transform.position;
            GameObject theBossCamera = Instantiate(bossCamera, oriPos, oriRot);
        }
    }
}
