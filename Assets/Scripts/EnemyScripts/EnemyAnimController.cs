using UnityEngine;

public class EnemyAnimController : MonoBehaviour {

    public Animator bodyAnim; // For Body Object
    public GameObject shieldAnim; // For Shield Object
    public PlayerTracker playerTracker;
    public Phase3 phase3Flu;
    public Animator fluBalls;

    void FireBullet(){
        playerTracker.Fire();
    }
    
    void StopShootAnim(){
        bodyAnim.SetBool("isShootingBool", false);
    }

    void DisableShieldAnim(){
        shieldAnim.SetActive(false);
    }

    void FluRain(){
        phase3Flu.Fire();
    }

    void FluStopRain(){
        fluBalls.SetBool("isCooldown", true);
    }
    
}