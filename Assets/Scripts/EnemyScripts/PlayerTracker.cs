﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTracker : MonoBehaviour
{
    public bool shooting = false;
    private Transform playerPos;

    //shooting vars
    public GameObject bullet;
    public float bulletSpeed;
    private float fireCD;
    public float fireTimer;

    //barrel vars
    private float lookAngle;
    [SerializeField]
    private GameObject barrel;
    private Transform barrelTransform;
    [SerializeField]
    private Transform barrelTip;

    public float distance;
    public Vector2 direction;

    public Animator bodyAnim;


    // Start is called before the first frame update
    void Start()
    {
        playerPos = GameObject.FindGameObjectWithTag("ThePlayer").GetComponent<Transform>();
        fireCD = 0f;
        barrelTransform = barrel.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        var heading = playerPos.position - transform.position;
        distance = heading.magnitude;
        direction = heading / distance;
        shooting = CanSeePlayer(distance, direction);

        if (shooting)
        {
            if (fireCD >= fireTimer)
            {
                // Play Shoot Anim
                bodyAnim.SetBool("isShootingBool", true);
            }
        }
        if (fireCD < fireTimer) {
            fireCD += Time.deltaTime;
        }

        //rotate barrelTip
        lookAngle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        barrelTransform.rotation = Quaternion.Euler(0f, 0f, lookAngle);
    }

    public void Fire()
    {
        fireCD = 0;
        GameObject bulletInstance = Instantiate(bullet, barrelTip.position, barrelTip.rotation);
        bulletInstance.GetComponent<Rigidbody2D>().AddForce(bulletInstance.GetComponent<Transform>().right * bulletSpeed);
    }

    bool CanSeePlayer(float distance, Vector2 direction) {
        bool val = false;
        RaycastHit2D ray = Physics2D.Raycast(transform.position, direction, distance, 1 << LayerMask.NameToLayer("Action"));
        if (ray)
        {
            if (ray.collider.gameObject.CompareTag("ThePlayer"))
            {
                val = true;
            }
        }
        return val;
    }
}
