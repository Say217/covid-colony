﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{
    public float walkSpeed;

    [HideInInspector]
    public bool mustPatrol = true;
    private bool mustTurn;
    private bool mustTurnAlt;

    public Rigidbody2D rb;
    public Transform groundCheckPos;
    public LayerMask groundLayer;
    public Transform wallCheckPos;
    public PlayerTracker playerTracker;

    public float flipTimer;
    private bool flipping;

    public RectTransform canvas;

    public Animator bodyAnim;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (mustPatrol)
        {
            bodyAnim.SetBool("isWalking", true);
            Patrol();
        }

        if (playerTracker.shooting)
        {
            bodyAnim.SetBool("isWalking", false);
            rb.velocity = rb.velocity * 0;
            if (playerTracker.direction.x > 0 && transform.localScale.x < 0)
            {
                transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
                walkSpeed *= -1;
                fixCanvas();

            }
            else if (playerTracker.direction.x < 0 && transform.localScale.x > 0)
            {
                transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
                walkSpeed *= -1;
                fixCanvas();
            }
        }
    }


    private void FixedUpdate()
    {
        if (mustPatrol)
        {
            TurnCheck();
        }
    }

    void Patrol()
    {
        if (mustTurn || mustTurnAlt)
        {
            bodyAnim.SetBool("isWalking", false);
            if (!flipping && !playerTracker.shooting)
            {
                StartCoroutine(Flip());
            }
        }

        if (mustPatrol && !playerTracker.shooting)
        {
            rb.velocity = new Vector2(walkSpeed * Time.fixedDeltaTime, rb.velocity.y);
        }
    }

    void TurnCheck()
    {
        mustTurn = !Physics2D.OverlapCircle(groundCheckPos.position, 0.1f, groundLayer);
        mustTurnAlt = Physics2D.OverlapCircle(wallCheckPos.position, 0.1f, groundLayer);
    }

    private IEnumerator Flip()
    {
        flipping = true;
        mustPatrol = false;
        rb.velocity = rb.velocity * 0;
        yield return new WaitForSeconds(flipTimer);
        transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
        fixCanvas();
        walkSpeed *= -1;
        TurnCheck();
        mustPatrol = true;
        flipping = false;
    }

    private void fixCanvas()
    {
        // Debug.Log("Flipping");
        canvas.localScale = new Vector2(canvas.localScale.x * -1, canvas.localScale.y);
    }
}
