﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    public float speed;
    public float damage;
    public string bulletType;
    private Rigidbody2D rb;
    // Start is called before the first frame update

    // Animations
    public GameObject playerHitAnim;

    void Start()
    { }

    private void Update()
    {
    }

    // Use for Pierce Bullet only
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Damage enemies with crit chance
        if (collision.gameObject.CompareTag("ThePlayer"))
        {
            collision.gameObject.GetComponent<PlayerHPController>().PlayerTakeDamage(damage, 0f);

            // Player Hit Anim
            Quaternion hitRot = Quaternion.identity;
            hitRot.eulerAngles = new Vector3(-45f, 0f, 0f);
            Vector3 hitPos = transform.position;
            GameObject hitAnimObject = Instantiate(playerHitAnim, hitPos, hitRot) as GameObject;

            Destroy(gameObject);
        }
        // Others Destroy
        else if (collision.gameObject.CompareTag("environment") || collision.gameObject.CompareTag("Moving Platform"))
        {
            // Debug.Log("bullet hit environment");
            Destroy(gameObject);
        }
    }

}
