﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Phase1OnTrigger : MonoBehaviour
{
    public bool playerIsHere = false;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("ThePlayer"))
        {
            Debug.Log("Player is Here");
            playerIsHere = true;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("ThePlayer"))
        {
            playerIsHere = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("ThePlayer"))
        {
            Debug.Log("Player is Here no more");
            playerIsHere = false;
        }
    }
}
