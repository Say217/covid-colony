﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushingBullet : MonoBehaviour
{
    public float speed;
    public string bulletType;
    private Rigidbody2D rb;
    // Start is called before the first frame update

    // Animations
    public GameObject playerHitAnim;

    void Start()
    { }

    private void Update()
    {
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Damage enemies with crit chance
        if (collision.gameObject.CompareTag("ThePlayer"))
        {
            return;
        }
        else
        {
            Destroy(gameObject);
        }
    }

}
