﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhaseController : MonoBehaviour
{
    public FluHPController HPC;
    public Phase2 phase2;
    public Phase3 phase3;
    private GameManager gm;

    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameManager>();
    }

    void Update()
    {
        if (HPC.hp <= 1200)
        {
            //enable phase 2
            phase2.onPhase2 = true;
        }

        if (HPC.hp <= 600)
        {
            //enable phase 3
            phase3.onPhase3 = true;
        }

        if (HPC.hp <= 0)
        {
            //enable Win
            gm.SetWin();
        }
        
    }
}
