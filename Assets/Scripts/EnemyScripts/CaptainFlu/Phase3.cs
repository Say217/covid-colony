﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Phase3 : MonoBehaviour
{
    public bool onPhase3;

    private float shootingCD = 0;
    public float shootingRate = 1f;

    public GameObject bullet;
    public float bulletSpeed;

    public Transform barrelTransform;

    public GameObject ballSprite;
    public Animator ballAnim;

    // Start is called before the first frame update
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {
        if (onPhase3)
        {
            if ((shootingCD >= shootingRate))
            {
                ballAnim.SetBool("isCooldown", false);
                // ballSprite.SetActive(false);
                ballAnim.SetTrigger("isShooting");
            }
        }
        if (shootingCD < shootingRate)
        {
            shootingCD += Time.deltaTime;
        }
    }

    public void Fire()
    {
        shootingCD = 0;
        GameObject bulletInstance = Instantiate(bullet, new Vector2 (barrelTransform.position.x + Random.Range(0,12),barrelTransform.position.y), barrelTransform.rotation);
        bulletInstance.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, bulletSpeed*-1));
    }
}
