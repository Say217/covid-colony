﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FluHPController : MonoBehaviour
{

    public float hp = 1500;
    public bool isPierceable = false; // One shot with pierce

    //GeneralSpawner mySpawner;
    //Obstacle obstacle;
    //public GameObject explosionParticle;
    //public Vector3 explosionCenter;

    public Slider hp1Slider;
    //public Slider hp2Slider;

    // Shield Animation
    //public bool isShieldedEnemy = false;
    //bool isShieldDestroyed = false;
    //public GameObject shieldSprite;
    //public Animator shieldAnim;
    private GameManager gm;

    void Start()
    {
        setHP1Bar(hp);
        gm = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameManager>();
    }

    void Update()
    {
        // Test damage code
        // if (Input.GetKeyDown(KeyCode.K)){
        //     TakeDamage(10f, 0.5f);
        // }

        // Move Spawn Anim
        //if(this.CompareTag("enemy") && spawnAnimObject != null){
        //    spawnAnimObject.transform.position = this.transform.position - new Vector3(0, 0.7f, 0);
        //}
    }

    public void TakeDamage(float damage, float critical, string bulletType)
    {
        float taken = Random.Range(0.9f * damage, 1.2f * damage);
        if (critical >= Random.Range(0f, 1f))
        {
            // Debug.Log("Critical!");
            taken = taken*2;
        }
        
        hp -= taken;
        // Debug.Log("HP Enemy: " + hp);
        setHP1Bar(hp);

        // Dies / destroyed
        if (hp <= 0){
            // Only Enemies have spawner
            float destroyDuration = 0f;
            //if(this.tag == "enemy"){
            //    // Dies Anim
            //    Quaternion dieRot = Quaternion.identity;
            //    dieRot.eulerAngles = new Vector3(0f, 0f, 0f);

            //    // If Shield Enemy (For now Sergeant Only)
            //    if(isShieldedEnemy){
            //        dieRot.eulerAngles = new Vector3(90f, 0f, 0f);
            //    }
            //    Vector3 diePos = transform.position;
            //    dieAnimObject = Instantiate(dieAnim, diePos, dieRot) as GameObject;

            //    mySpawner.spawnedDestroyed();

            //} else if(this.tag == "obstacle"){
            //    obstacle.obstacleDestroyed();
            //    Vector2 explosionPos = transform.position + explosionCenter;
            //    GameObject explosion = Instantiate(explosionParticle, explosionPos, transform.rotation) as GameObject;
            //    // ParticleSystem explosionPart = explosion.GetComponent<ParticleSystem>();
                // destroyDuration = explosionPart.duration + explosionPart.startLifetime;
            //}
            gm.SetWin();
            Destroy(this.gameObject, destroyDuration);
        }
    }

    //public void setSpawner(GeneralSpawner spawner){
    //    mySpawner = spawner.GetComponent<EnemySpawner>();
    //    // Debug.Log("Spawner set");
    //}

    //public GeneralSpawner getMySpawner(){
    //    return mySpawner;
    //}

    private void setHP1Bar(float val)
    {
        if (val < 0)
        {
            val = 0;
        }
        hp1Slider.value = val;
    }
}
