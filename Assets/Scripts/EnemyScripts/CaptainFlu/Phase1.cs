﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Phase1 : MonoBehaviour
{
    public GameObject barrel1;
    public GameObject barrel2;
    public GameObject barrel3;

    private Phase1OnTrigger barrel1Trigger;
    private Phase1OnTrigger barrel2Trigger;
    private Phase1OnTrigger barrel3Trigger;
    private Transform barrel1Transform;
    private Transform barrel2Transform;
    private Transform barrel3Transform;


    private float shootingCD = 0;
    public float shootingRate = 2;
    private int shootWhere = 1;
    private int shootCounter = 0;

    private float burstCD = 0;
    public float burstRate = 0.3f;

    public GameObject bullet;
    public float bulletSpeed;


    // Start is called before the first frame update
    void Start()
    {
        barrel1Trigger = barrel1.GetComponent<Phase1OnTrigger>();
        barrel2Trigger = barrel2.GetComponent<Phase1OnTrigger>();
        barrel3Trigger = barrel3.GetComponent<Phase1OnTrigger>();

        barrel1Transform = barrel1.GetComponent<Transform>();
        barrel2Transform = barrel2.GetComponent<Transform>();
        barrel3Transform = barrel3.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (barrel1Trigger.playerIsHere)
        {
            if (shootingCD < shootingRate)
            {
                shootWhere = 1;
            }
        }
        else if (barrel2Trigger.playerIsHere)
        {
            if (shootingCD < shootingRate)
            {
                shootWhere = 2;
            }
        }
        else if (barrel3Trigger.playerIsHere)
        {
            if (shootingCD < shootingRate)
            {
                shootWhere = 3;
            }
        }
        if ((shootingCD >= shootingRate) && (burstCD>=burstRate))
        {
            if(shootCounter < 3)
            {
                Fire();
                shootCounter++;
                burstCD = 0;
            }
            else
            {
                shootCounter = 0;
                shootingCD = 0f;
            }
        }
        if (shootingCD < shootingRate)
        {
            shootingCD += Time.deltaTime;
        }
        if (burstCD < burstRate)
        {
            burstCD += Time.deltaTime;
        }
    }

    void Fire()
    {
        if(shootWhere == 1)
        {
            GameObject bulletInstance = Instantiate(bullet, barrel1Transform.position, barrel1Transform.rotation);
            bulletInstance.GetComponent<Rigidbody2D>().AddForce(new Vector2(bulletSpeed*-1, 0f));
        }
        if (shootWhere == 2)
        {
            GameObject bulletInstance = Instantiate(bullet, barrel2Transform.position, barrel2Transform.rotation);
            bulletInstance.GetComponent<Rigidbody2D>().AddForce(new Vector2(bulletSpeed * -1, 0f));

        }
        if (shootWhere == 3)
        {
            GameObject bulletInstance = Instantiate(bullet, barrel3Transform.position, barrel3Transform.rotation);
            bulletInstance.GetComponent<Rigidbody2D>().AddForce(new Vector2(bulletSpeed * -1, 0f));
        }
    }
}
