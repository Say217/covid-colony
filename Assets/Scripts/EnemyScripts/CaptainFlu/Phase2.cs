﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Phase2 : MonoBehaviour
{
    public bool onPhase2;

    public Transform barrel1Transform;
    public Transform barrel2Transform;
    public Transform barrel3Transform;

    public Phase1OnTrigger barrel1Trigger;
    public Phase1OnTrigger barrel2Trigger;
    public Phase1OnTrigger barrel3Trigger;

    private float shootingCD = 0;
    public float shootingRate = 5;
    private int shootWhere = 1;

    public GameObject bullet;
    public float bulletSpeed;

    // Start is called before the first frame update
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {
        if (onPhase2)
        {
            if (barrel1Trigger.playerIsHere)
            {
                shootWhere = 1;
            }
            else if (barrel2Trigger.playerIsHere)
            {
                shootWhere = 2;
            }
            else if (barrel3Trigger.playerIsHere)
            {
                shootWhere = 3;
            }
            if ((shootingCD >= shootingRate))
            {
                Fire();
                shootingCD = 0;
            }
        }
        if (shootingCD < shootingRate)
        {
            shootingCD += Time.deltaTime;
        }
    }

    void Fire()
    {
        if (shootWhere == 1)
        {
            GameObject bulletInstance = Instantiate(bullet, barrel1Transform.position, barrel1Transform.rotation);
            bulletInstance.GetComponent<Rigidbody2D>().AddForce(new Vector2(bulletSpeed * -1, 0f));
        }
        if (shootWhere == 2)
        {
            GameObject bulletInstance = Instantiate(bullet, barrel2Transform.position, barrel2Transform.rotation);
            bulletInstance.GetComponent<Rigidbody2D>().AddForce(new Vector2(bulletSpeed * -1, 0f));

        }
        if (shootWhere == 3)
        {
            GameObject bulletInstance = Instantiate(bullet, barrel3Transform.position, barrel3Transform.rotation);
            bulletInstance.GetComponent<Rigidbody2D>().AddForce(new Vector2(bulletSpeed * -1, 0f));
        }
    }
}
