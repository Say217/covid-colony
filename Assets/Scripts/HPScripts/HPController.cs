﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HPController : MonoBehaviour
{

    public float hp = 100;
    public bool isPierceable = false; // One shot with pierce

    GeneralSpawner mySpawner;
    Obstacle obstacle;
    public GameObject explosionParticle;
    public Vector3 explosionCenter;

    public Slider hp1Slider;
    public Slider hp2Slider;

    // Shield Animation
    public bool isShieldedEnemy = false;
    bool isShieldDestroyed = false;
    public GameObject shieldSprite;
    public Animator shieldAnim;

    // Enemy Spawn Animations
    public GameObject spawnAnim;
    public GameObject dieAnim;
    GameObject spawnAnimObject;
    GameObject dieAnimObject;

    private void Awake() {
        if(this.tag == "obstacle"){
            obstacle = GetComponent<Obstacle>();
        }

        if(this.CompareTag("enemy")){
            // Spawn Anim
            Quaternion spawnRot = Quaternion.identity;
            spawnRot.eulerAngles = new Vector3(90f, 0f, 0f);
            Vector3 spawnPos = transform.position;
            spawnPos = spawnPos - new Vector3(0, 0.7f, 0);
            spawnAnimObject = Instantiate(spawnAnim, spawnPos, spawnRot) as GameObject;
        }
    }
    private void Start()
    {
        setHP2Bar(hp);
        setHP1Bar(hp);
    }

    void Update()
    {
        // Test damage code
        // if (Input.GetKeyDown(KeyCode.K)){
        //     TakeDamage(10f, 0.5f);
        // }

        // Move Spawn Anim
        if(this.CompareTag("enemy") && spawnAnimObject != null){
            spawnAnimObject.transform.position = this.transform.position - new Vector3(0, 0.7f, 0);
        }
    }

    public void TakeDamage(float damage, float critical, string bulletType)
    {   
        // Only obstacles can be pierced!!
        if(this.tag == "obstacle" && isPierceable){
            if(bulletType == "pierce"){
                obstacle.obstacleDestroyed();
                Vector2 explosionPos = transform.position + explosionCenter;
                GameObject explosion = Instantiate(explosionParticle, explosionPos, transform.rotation) as GameObject;
                Destroy(this.gameObject);
                return;
            } else{
                return;
            }
        }

        float taken = Random.Range(0.9f * damage, 1.2f * damage);
        if (critical >= Random.Range(0f, 1f))
        {
            // Debug.Log("Critical!");
            taken = taken*2;
        }
        
        hp -= taken;
        // Debug.Log("HP Enemy: " + hp);
        setHP2Bar(hp);
        setHP1Bar(hp);

        // Dies / destroyed
        if (hp <= 0){
            // Only Enemies have spawner
            float destroyDuration = 0f;
            if(this.tag == "enemy"){
                // Dies Anim
                Quaternion dieRot = Quaternion.identity;
                dieRot.eulerAngles = new Vector3(0f, 0f, 0f);

                // If Shield Enemy (For now Sergeant Only)
                if(isShieldedEnemy){
                    dieRot.eulerAngles = new Vector3(90f, 0f, 0f);
                }
                Vector3 diePos = transform.position;
                dieAnimObject = Instantiate(dieAnim, diePos, dieRot) as GameObject;

                mySpawner.spawnedDestroyed();

            } else if(this.tag == "obstacle"){
                obstacle.obstacleDestroyed();
                Vector2 explosionPos = transform.position + explosionCenter;
                GameObject explosion = Instantiate(explosionParticle, explosionPos, transform.rotation) as GameObject;
                // ParticleSystem explosionPart = explosion.GetComponent<ParticleSystem>();
                // destroyDuration = explosionPart.duration + explosionPart.startLifetime;
            }
            Destroy(this.gameObject, destroyDuration);
        }
    }

    public void setSpawner(GeneralSpawner spawner){
        mySpawner = spawner.GetComponent<EnemySpawner>();
        // Debug.Log("Spawner set");
    }

    public GeneralSpawner getMySpawner(){
        return mySpawner;
    }

    private void setHP1Bar(float val)
    {
        if (val < 0)
        {
            val = 0;
        }
        if (val < 100)
        {

            hp1Slider.value = val;
        }
        else if (val >= 100)
        {
            val = 100;
            hp1Slider.value = val;
        }
    }
    private void setHP2Bar(float val)
    {
        if (val < 100)
        {
            val = 0;
            if(isShieldedEnemy && !isShieldDestroyed){
                isShieldDestroyed = true;
                // destroy shield
                shieldSprite.SetActive(false);
                shieldAnim.SetTrigger("destroyShield");
            }
        }
        else
        {
            val -= 100;
        }
        hp2Slider.value = val;
    }
}
