using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerHPController : MonoBehaviour {
    // Game Manager Vars
    private GameManager gm;
    public static float hp;
    public float maskshield;
    public float maxShield;

    [SerializeField]
    private float maskOnTime;
    private float maskTimer;
    private bool maskOn;

     // HP UI Vars
    public Slider hp1Slider;
    public Slider hp2Slider;
    public Slider shieldSlider;

    // Animations
    public GameObject loseHeartAnim;


    private void Start()
    {
        gm = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameManager>();
        if (this.gameObject.CompareTag("ThePlayer")) // Make sure tag
        {
            if(!GameManager.hasDied){
                // Set Spawn Point, comment kalau lagi gaa perlu
                if(SceneManager.GetActiveScene().name == "TutorialScene"){
                    gm.lastCheckpointPos = new Vector2(-5f, -3.05f);
                }
                if(SceneManager.GetActiveScene().name == "Level1Scene"){
                    gm.lastCheckpointPos = new Vector2(-5f, -2.65f);
                    // gm.lastCheckpointPos = new Vector2(190.01f, 81.28f);
                }

                // Set starting max HP and crit chance at start of level
                GameManager.startingMaxHp = GameManager.maxHp;
                GameManager.startingCritChance = GameManager.critChance;
                GameManager.startingCheckpointPos = gm.lastCheckpointPos;
            } else{
                // Player Hit Anim
                Quaternion heartRot = Quaternion.identity;
                heartRot.eulerAngles = new Vector3(-90f, 0f, 0f);
                Vector3 heartPos = transform.position;
                heartPos = heartPos + new Vector3(0, 1f, 0);
                GameObject loseHeartAnimObject = Instantiate(loseHeartAnim, heartPos, heartRot) as GameObject;
                loseHeartAnimObject.transform.parent = this.transform;
            }

            hp = GameManager.maxHp;
            shieldSlider.value = 0;
            setHP2Bar(hp);
            setHP1Bar(hp);

            // Debug.Log("Current HP: " + hp);
            // Debug.Log("Remaining Lives: " + GameManager.lives);
            // Debug.Log("Crit chance total: " + GameManager.critChance);

            // Debug.Log("Starting HP: " + GameManager.startingMaxHp);
            // Debug.Log("Starting Crit chance: " + GameManager.startingCritChance);
        } else {
            Debug.LogError("[PlayerHPScript] Should only be used on player");
        }
    }

    void Update()
    {
        // Test damage code, COMMENT IF NOT NEEDED!!
        // if (Input.GetKeyDown(KeyCode.K)){
        //     PlayerTakeDamage(10f, 0.5f);
        // }

        if (maskOn)
        {
            maskTimer -= Time.deltaTime;
        }

        if (maskTimer <= 0 && maskOn)
        {
            maskOn = false;
            maskshield = 0;
            shieldSlider.value = maskshield;
            // Debug.Log("maskShield OFF");
        }

        setHP2Bar(hp);
        setHP1Bar(hp);
    }

    public void MaskOn()
    {
        maskshield = maxShield;
        maskTimer = maskOnTime;
        maskOn = true;
        shieldSlider.value = maskshield;
        // Debug.Log("maskShield = " + maskshield);
    }

    public void PlayerTakeDamage(float damage, float critical)
    {
        float taken = Random.Range(0.9f * damage, 1.2f * damage);
        if (critical >= Random.Range(0f, 1f))
        {
            // Debug.Log("Critical!");
            taken = taken*2;
        }
        // Debug.Log("taken = " + taken);
        maskshield -= taken;
        if (maskshield <= taken)
        {
            // Debug.Log("Case 1");
            taken -= maskshield;
            maskshield = 0;
        }
        else{
            // Debug.Log("Case 2");
            maskshield -= taken;
            taken = 0;
        }
        // Debug.Log("maskShield = " + maskshield);
        hp -= taken;

        shieldSlider.value = maskshield;
        setHP2Bar(hp);
        setHP1Bar(hp);
        // Debug.Log("Player HP: " + hp);

        // Player dies
        if (GameManager.lives > 0 && hp <= 0) {
            GameManager.lives -= 1;
            GameManager.hasDied = true;
            GameManager.isStillInEncounter = false;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        // Game Over. gak bisa elif
        if (GameManager.lives <= 0)
        {
            // Debug.Log("Game Over");
            if(SceneManager.GetActiveScene().name == "TutorialScene"){
                gm.setLastLevel(0);
            }
            if(SceneManager.GetActiveScene().name == "Level1Scene"){
                gm.setLastLevel(1);
            }
            if(SceneManager.GetActiveScene().name == "VSFlu"){
                gm.setLastLevel(1);
            }
            SceneManager.LoadScene("GameOver");
        }

    }

    private void setHP1Bar(float val)
    {
        if(val < 0)
        {
            val = 0;
        }
        if(val < 100) {

            hp1Slider.value = val;
        }
        else if(val >= 100)
        {
            val = 100;
            hp1Slider.value = val;
        }
    }
    private void setHP2Bar(float val)
    {
        if (val < 100)
        {
            val = 0;
        }
        else
        {
            val -= 100;
        }
        hp2Slider.value = val;
    }
}