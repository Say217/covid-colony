﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossTriggerScene : MonoBehaviour
{
    private GameManager gm;

    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if(collision.gameObject.tag == "ThePlayer"){
            if(SceneManager.GetActiveScene().name == "Level1Scene"){
                gm.lastCheckpointPos = new Vector3(202.55f, 83.6f, 0);
                SceneManager.LoadScene("VSFlu");
            }
        }
    }
}
