﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    private GameManager gm;
    public int levelNum;
    public int obstacleNum;
    public bool isPiercable;

    private bool isDestroyed;

    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameManager>();
        if(isPiercable){
            isDestroyed = gm.getPierceObstacleDestroyed(levelNum, obstacleNum);
        } else{
            isDestroyed = gm.getObstacleDestroyed(levelNum, obstacleNum);
        }

        if(isDestroyed){
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    public void obstacleDestroyed(){
        if(isPiercable){
            gm.setPierceObstacleDestroyed(levelNum, obstacleNum);
        } else{
            gm.setObstacleDestroyed(levelNum, obstacleNum);
        }
    }
}
