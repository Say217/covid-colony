﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialTrigger : MonoBehaviour
{
    public int tutorialNum;
    public GameObject tutorialMenu;
    public string tutorialSentence;
    bool isTutorialCompleted = false;
    bool isTriggered = false;
    public Sprite objectSprite;
    public static bool isShown = false;
    public bool hasImage = false;
    public RectTransform imageRect;


    // Start is called before the first frame update
    void Start()
    {
        GameManager.isTutorial = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if(!isTutorialCompleted && collision.gameObject.tag == "ThePlayer"){
            isTriggered = true;
            ShowQuiz();
        }
    }

    void ShowQuiz(){
        tutorialMenu.SetActive(true);
        Time.timeScale = 0f;
        isShown = true;
        AssignVars();
    }

    void AssignVars(){
        Text tutSentence = tutorialMenu.transform.Find("Sentence").gameObject.GetComponent<Text>();
        tutSentence.text = tutorialSentence;

        Image tutImage = tutorialMenu.transform.Find("Object Image").gameObject.GetComponent<Image>();
        if(hasImage){
            tutImage.sprite = objectSprite;
            imageRect.sizeDelta = new Vector2 (tutImage.sprite.rect.width/2,tutImage.sprite.rect.height/2);
            tutImage.gameObject.SetActive(true);
        }
    }

    void HideQuiz(){
        tutorialMenu.SetActive(false);
        Time.timeScale = 1f;
        isShown = false;
        isTutorialCompleted = true;
    }

    public void Continue(){
        if(!isTriggered){
            return;
        }
        tutorialMenu.SetActive(false);

        Time.timeScale = 1f;
        HideQuiz();
    }
}
