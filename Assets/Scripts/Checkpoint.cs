﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Checkpoint : MonoBehaviour
{
    public Canvas myCanvas;
    public float fillSpeed;
    public int levelNum;
    public int checkpointNum;
    private GameManager gm;
    private Image banner;
    private bool isReached = false;
    private bool isTriggered = false;

    private void Start()
    {
        gm = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameManager>();
        banner = myCanvas.transform.Find("Banner").gameObject.GetComponent<Image>();
        isReached = gm.getCheckpointsReached(levelNum, checkpointNum);

        if(isReached){
            banner.fillAmount = 1f;
        }
    }

    private void Update() {
        if(isTriggered){
            SetBanner();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("ThePlayer"))
        {
            gm.lastCheckpointPos = transform.position;
            isTriggered = true;
            gm.setCheckpointsReached(levelNum, checkpointNum);
        }
    }

    void SetBanner(){
        if(banner.fillAmount >= 1f){
            isTriggered = false;
            return;
        }
        banner.fillAmount += fillSpeed * Time.deltaTime;
    }
}
