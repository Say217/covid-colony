﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public Transform pos1;
    public Transform pos2;
    public float moveSpeed;

    Vector3 nextPos;
    void Start()
    {
        transform.position = pos1.position;
        nextPos = pos1.position;
    }

    void Update()
    {
        if(transform.position == pos1.position){
            nextPos = pos2.position;
        }else if(transform.position == pos2.position){
            nextPos = pos1.position;
        }

        transform.position = Vector3.MoveTowards(transform.position, nextPos, moveSpeed * Time.deltaTime);

    }
}
