﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void PlayGame(){
        SceneManager.LoadScene("Level1Intro");
    }

    public void Tutorial(){
        SceneManager.LoadScene("TutorialScene");
    }

    public void Credits(){
        Debug.Log("CREDITS!!!");
        // SceneManager.LoadScene("TutorialScene");
    }

    public void QuitGame(){
        Debug.Log("QUITTING GAME...");
        Application.Quit();
    }
}
