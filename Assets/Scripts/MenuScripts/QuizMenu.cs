﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class QuizMenu : MonoBehaviour
{
    public int numberOfQuestions;
    public string[] question;
    public string[] aOption;
    public string[] bOption;
    public string[] cOption;
    public string[] dOption;
    public GameObject quizMenuUI;
    public string[] correctAnswer;
    public bool isMaxHP; // True yang nambah max HP
    public GameObject artefactTrigger;
    public Sprite completedSprite;

    bool isInteracted = false; // Player interacts
    bool isPlayerHover = false; // Player on top
    int quizIndex = 0;
    public static bool isShown = false; // Perlu buat diakses dari controls lain

    // Game Manager Vars
    private GameManager gm;
    private bool isCompleted = false;
    public int levelNum;
    public int artefactNum;

    // Animation
    public GameObject getArtefactAnim;

    private PlayerHUD playerHUD;

    private void Start() {
        gm = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameManager>();
        playerHUD = GameObject.FindGameObjectWithTag("Player HUD").GetComponent<PlayerHUD>();
        isCompleted = gm.getArtefactsCompleted(levelNum, artefactNum);

        if(isCompleted){
            this.GetComponent<SpriteRenderer>().sprite = completedSprite;
            // Destroy(gameObject); // Kalo jadinya gak mau diilangin, mungkin tetep ada artefaknya bisa ubah di sini
        }
    }

    void Update()
    {
        if(!isCompleted && isPlayerHover && !GameManager.isStillInEncounter && Input.GetKeyDown(KeyCode.E)){
            if(!isShown){
                ShowQuiz();
            } 
            // else {
            //     HideQuiz();
            // }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if(collision.CompareTag("ThePlayer")){
            isPlayerHover = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if(collision.CompareTag("ThePlayer")){
            isPlayerHover = false;
        }
    }

    void ShowQuiz(){
        AssignVars();

        quizMenuUI.SetActive(true);
        Time.timeScale = 0f;
        isInteracted = true;
        isShown = true;
    }

    void HideQuiz(){
        quizMenuUI.SetActive(false);
        Time.timeScale = 1f;
        isInteracted = false;
        isShown = false;
    }

    void AssignVars(){
        Text questionText = quizMenuUI.transform.Find("Question").gameObject.GetComponent<Text>();
        questionText.text = question[quizIndex];

        Text aText = quizMenuUI.transform.Find("A Option").gameObject.GetComponent<Text>();
        aText.text = aOption[quizIndex];

        Text bText = quizMenuUI.transform.Find("B Option").gameObject.GetComponent<Text>();
        bText.text = bOption[quizIndex];

        Text cText = quizMenuUI.transform.Find("C Option").gameObject.GetComponent<Text>();
        cText.text = cOption[quizIndex];

        Text dText = quizMenuUI.transform.Find("D Option").gameObject.GetComponent<Text>();
        dText.text = dOption[quizIndex];
    }

    bool incrementQuiz(){
        quizIndex++;

        // Kalo masih ada pertanyan
        if(quizIndex < numberOfQuestions){
            AssignVars();
            return true;
        }

        return false;
    }

    public void OptionA(){
        if(isInteracted){
            // Debug.Log("A Selected " + this.gameObject.name);
            if(correctAnswer[quizIndex] == "a"){
                if(incrementQuiz()){
                    return;
                }
                Correct();
            } else{
                Wrong();
            }
        }
    }

    public void OptionB(){
        if(isInteracted){
            // Debug.Log("B Selected " + this.gameObject.name);
            if(correctAnswer[quizIndex] == "b"){
                if(incrementQuiz()){
                    return;
                }
                Correct();
            } else{
                Wrong();
            }
        }
    }

    public void OptionC(){
        if(isInteracted){
            // Debug.Log("C Selected " + this.gameObject.name);
            if(correctAnswer[quizIndex] == "c"){
                if(incrementQuiz()){
                    return;
                }
                Correct();
            } else{
                Wrong();
            }
        }
    }

    public void OptionD(){
        if(isInteracted){
            // Debug.Log("D Selected " + this.gameObject.name);
            if(correctAnswer[quizIndex] == "d"){
                if(incrementQuiz()){
                    return;
                }
                Correct();
            } else{
                Wrong();
            }
        }
    }

    void Correct(){
        // Debug.Log("Correct Answer");
        HideQuiz(); // TODO: Add SFX while waiting and Correct/Wrong Menu

        // Increase Health or Increase Crit Chance
        if(isMaxHP){
            GameManager.maxHp += 10;
        } else {
            GameManager.critChance += 0.03f;
        }

        // Reset Lives and Health
        GameManager.lives = 3;
        PlayerHPController.hp = GameManager.maxHp;
        gm.lastCheckpointPos = artefactTrigger.GetComponent<EncounterSpawner>().winCheckpoint;
        playerHUD.UpdateLives();

        // Play Anim Success
        // Player Hit Anim
        Quaternion getArtefactRot = Quaternion.identity;
        getArtefactRot.eulerAngles = new Vector3(0f, 0f, 0f);
        Vector3 getPos = transform.position;
        GameObject hitAnimObject = Instantiate(getArtefactAnim, getPos, getArtefactRot) as GameObject;

        ArtefactsUsed();
    }

    void Wrong(){
        // Debug.Log("Wrong Answer");

        HideQuiz(); // TODO: Add SFX while waiting and Correct/Wrong Menu

        if(GameManager.isTutorial){
            GameManager.lives = 0;
            GameManager.hasDied = true;
            gm.setLastLevel(0);
            SceneManager.LoadScene("GameOver");
        }

        // Encounter Begins
        artefactTrigger.GetComponent<EncounterSpawner>().ForceSpawn();

        ArtefactsUsed();
    }

    void ArtefactsUsed(){
        gm.setArtefactsCompleted(levelNum, artefactNum);
        isCompleted = gm.getArtefactsCompleted(levelNum, artefactNum);
        this.GetComponent<SpriteRenderer>().sprite = completedSprite;
        // Destroy(gameObject);
    }
}
