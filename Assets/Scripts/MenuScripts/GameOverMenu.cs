﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverMenu : MonoBehaviour
{
    // Game Manager Vars
    private GameManager gm;

    private void Start() {
        gm = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameManager>();
        gm.resetAll();
    }

    public void MainMenu(){
        SceneManager.LoadScene("MainMenu");
    }

    public void Restart(){
        // Load Last Level
        // Debug.Log("Restarting Level...");
        if(gm.getLastLevel() == 0){
            SceneManager.LoadScene("TutorialScene");
        }
        else if(gm.getLastLevel() == 1){
            SceneManager.LoadScene("Level1Scene");
        }

    }
}
