﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHUD : MonoBehaviour
{
    // Game Manager Vars
    private GameManager gm;

    private void Start() {
        gm = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameManager>();
        UpdateLives();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateLives(){
        GameObject heart1 = transform.Find("Heart 1").gameObject;
        GameObject heart2 = transform.Find("Heart 2").gameObject;
        GameObject heart3 = transform.Find("Heart 3").gameObject;
        // Debug.Log("Lives Updated");

        if(GameManager.lives == 3){
            heart1.SetActive(true);
            heart2.SetActive(true);
            heart3.SetActive(true);
        } else if(GameManager.lives == 2){
            heart1.SetActive(true);
            heart2.SetActive(true);
            heart3.SetActive(false);
        } else if (GameManager.lives == 1){
            heart1.SetActive(true);
            heart2.SetActive(false);
            heart3.SetActive(false);
        }
    }
}
