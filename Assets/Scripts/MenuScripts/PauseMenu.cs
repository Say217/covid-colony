﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    // Game Manager Vars
    private GameManager gm;

    public static bool GameIsPaused = false;
    public GameObject pauseMenuUI;

    private void Start() {
        gm = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameManager>();
        // Debug.Log("pause init");
    }

    void Update() {
        if(Input.GetKeyDown(KeyCode.Escape)){
            // Debug.Log("masuk");
            if(GameIsPaused){
                Resume();
            }else{
                Pause();
            }
        }
    }

    public void MainMenu(){
        gm.resetAll();
        Time.timeScale = 1f;
        GameIsPaused = false;
        SceneManager.LoadScene("MainMenu");
    }

    void Pause(){
        SetStats();
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void Resume(){
        pauseMenuUI.SetActive(false);
        if(QuizMenu.isShown || TutorialTrigger.isShown){
            Time.timeScale = 0f;
        }else{
            Time.timeScale = 1f;
        }
        GameIsPaused = false;
    }

    public void SetStats(){
        Text maxHPText = pauseMenuUI.transform.Find("MaxHP").gameObject.GetComponent<Text>();
        Text critText = pauseMenuUI.transform.Find("CritChance").gameObject.GetComponent<Text>();

        float critPercentage = GameManager.critChance * 100;

        maxHPText.text = "Maximum HP: " + GameManager.maxHp.ToString();
        critText.text = "Critical Chance: " + critPercentage.ToString() +"%";
    }
}
