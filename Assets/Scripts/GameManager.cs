﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    public Vector2 lastCheckpointPos; //Default value gak ngefek, liat PlayerHPController Start()
    public static float maxHp = 100; // TODO: Load dari save file
    public static int lives = 3;
    public static float critChance = 0.1f; // TODO: Load dari save file

    // Buat tau original state
    public static float startingMaxHp = maxHp;
    public static float startingCritChance = critChance;
    public static Vector2 startingCheckpointPos = new Vector2(0, 0);
    public static bool hasDied = false; // True kalo udh mati di level, pas false dipake buar set original state
    public static bool isStillInEncounter = false;
    public static bool isTutorial = false;
    int lastLevel = -1; // Buat dipanggil dari scene game over

    int numOfLevels = 1;

    // Encounter Bools
    bool[,] encounterFinished;
    int maxNumOfEncounter = 10; // Max Num of Encounter from all levels (including quiz)

    // Obstacles bools
    bool[,] obstacleDestroyed;
    int maxNumOfObstacle = 15;

    // Pierce Obstacle Bools
    bool[,] pierceObstacleDestroyed;
    int maxNumOfPierceObstacle = 5;

    // Artefacts Bools
    bool[,] artefactsCompleted;
    int maxNumOfArtefacts = 5;

    // Checkpoint bools
    bool[,] checkpointsReached;
    int maxNumOfCheckpoints = 20;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }
        resetLevels();
    }

    private void Start() {
    }

    public void setLastLevel(int level){
        lastLevel = level;
    }

    public int getLastLevel(){
        return lastLevel;
    }

    public void resetAll(){
        resetLevels(); // set booleans

        // Take starting vars
        maxHp = startingMaxHp;
        critChance = startingCritChance;
        lives = 3;

        hasDied = false;
        isTutorial = false;
    }
    
    private void resetLevels(){

        encounterFinished = new bool[numOfLevels,maxNumOfEncounter];
        for(int i = 0; i<numOfLevels; i++){
            for(int j = 0; j<maxNumOfEncounter; j++){
                encounterFinished[i,j] = false;
            }
        }

        obstacleDestroyed = new bool[numOfLevels,maxNumOfObstacle];
        for(int i = 0; i<numOfLevels; i++){
            for(int j = 0; j<maxNumOfObstacle; j++){
                obstacleDestroyed[i,j] = false;
            }
        }

        pierceObstacleDestroyed = new bool[numOfLevels,maxNumOfPierceObstacle];
        for(int i = 0; i<numOfLevels; i++){
            for(int j = 0; j<maxNumOfPierceObstacle; j++){
                pierceObstacleDestroyed[i,j] = false;
            }
        }

        artefactsCompleted = new bool[numOfLevels,maxNumOfArtefacts];
        for(int i = 0; i<numOfLevels; i++){
            for(int j = 0; j<maxNumOfArtefacts; j++){
                artefactsCompleted[i,j] = false;
            }
        }

        checkpointsReached = new bool[numOfLevels,maxNumOfCheckpoints];
        for(int i = 0; i<numOfLevels; i++){
            for(int j = 0; j<maxNumOfCheckpoints; j++){
                checkpointsReached[i,j] = false;
            }
        }
    }

    // Encounter Finished Setter Getter
    public bool getEncounterFinished(int i, int j){
        return encounterFinished[i,j];
    }

    public void setEncounterFinished(int i, int j){
        encounterFinished[i,j] = true;
    }

    // Obstacle Destroyed Setter Getter
    public bool getObstacleDestroyed(int i, int j){
        return obstacleDestroyed[i,j];
    }

    public void setObstacleDestroyed(int i, int j){
        obstacleDestroyed[i,j] = true;
    }

    // Pierce Obstacle Destroyed Setter Getter
    public bool getPierceObstacleDestroyed(int i, int j){
        return pierceObstacleDestroyed[i,j];
    }

    public void setPierceObstacleDestroyed(int i, int j){
        pierceObstacleDestroyed[i,j] = true;
    }

    // Artefacts Completed Setter Getter
    public bool getArtefactsCompleted(int i, int j){
        return artefactsCompleted[i,j];
    }

    public void setArtefactsCompleted(int i, int j){
        artefactsCompleted[i,j] = true;
    }

    // Checkpoints Reached Setter Getter
    public bool getCheckpointsReached(int i, int j){
        return checkpointsReached[i,j];
    }

    public void setCheckpointsReached(int i, int j){
        checkpointsReached[i,j] = true;
    }

    public void SetWin(){
        SceneManager.LoadScene("WinScene");
    }
}
