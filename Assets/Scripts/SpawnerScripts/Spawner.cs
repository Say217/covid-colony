﻿using UnityEngine;

// Default Spawner class for any prefab in spawn manager.
// Use encounter tag for enemy spawner.
public class GeneralSpawner : MonoBehaviour
{
    // An instance of the ScriptableObject.
    public SpawnManager spawnManager;

    // This will be appended to the name of the created entities and increment when each is created.
    protected int instanceNumber = 1;
    protected bool isSpawned = false; // Keep track spawned or not
    protected int curSpawned = -1;
    protected GameObject[] spawnedEntities;

    // Spawn entities from spawn manager
    protected void SpawnEntities(){
        isSpawned = true;
        curSpawned = spawnManager.numberOfPrefabsToCreate;
        // Debug.Log("Spawned");

        int curSpawnPointIndex = 0;
        spawnedEntities = new GameObject[spawnManager.numberOfPrefabsToCreate];

        for(int i = 0; i < spawnManager.numberOfPrefabsToCreate; i++){
            int curIndex = spawnManager.prefabIndexes[i];
            GameObject currentEntity = Instantiate(spawnManager.prefab[curIndex], spawnManager.spawnPoints[curSpawnPointIndex], Quaternion.identity);
            currentEntity.transform.localScale = spawnManager.scaleSize[curSpawnPointIndex];
            currentEntity.transform.localEulerAngles = spawnManager.rotationSize[curSpawnPointIndex];

            if(currentEntity.tag == "enemy"){
                currentEntity.GetComponent<HPController>().setSpawner(this);
                // Debug.Log("enemies");
            }
            // Rename Spawned Object
            currentEntity.name = spawnManager.prefab[curIndex].name + instanceNumber;
            instanceNumber++;

            // Spawn more than location
            curSpawnPointIndex = (curSpawnPointIndex + 1) % spawnManager.spawnPoints.Length;

            spawnedEntities[i] = currentEntity;
        }
    }

    // Called from spawned entities when destroyed
    public virtual void spawnedDestroyed(){
        this.curSpawned--;
        // Debug.Log("Spawned Destroyed, remaining: " + this.curSpawned);
    }
}
