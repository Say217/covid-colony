﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpawnManager", menuName = "ScriptableObjects/Spawner", order = 0)]
public class SpawnManager : ScriptableObject {
    // TODO: Make multiple types of enemies spawn
    public GameObject[] prefab;

    public int numberOfPrefabsToCreate;
    public Vector3[] spawnPoints;
    public Vector3[] scaleSize;
    public Vector3[] rotationSize;
    public int[] prefabIndexes;
}
