using UnityEngine;

public class EnemySpawner : GeneralSpawner {

    // Game Manager Vars
    private GameManager gm;
    public int levelNum;
    public int encounterNum;

    EncounterSpawner encounterSpawner;
    bool isEncounterFinished = false;

    private void Awake() {
        encounterSpawner = GetComponent<EncounterSpawner>();
    }

    private void Start() {
        gm = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameManager>();
        isEncounterFinished = gm.getEncounterFinished(levelNum, encounterNum);
    }

    public bool getIsSpawned(){
        return isSpawned;
    }

    public void DoSpawnEntities(){
        SpawnEntities();
    }

    public override void spawnedDestroyed(){
        this.curSpawned--;
        // Debug.Log("Spawned Destroyed, remaining: " + this.curSpawned);

        // Called when encounter is done
        if(!isEncounterFinished && this.tag == "encounter" && isSpawned && this.curSpawned == 0){
            encounterSpawner.FinishEncounter();
        }
    }
}