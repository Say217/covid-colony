using UnityEngine;

public class EncounterSpawner : GeneralSpawner {

    // Game Manager Vars
    private GameManager gm;
    private PlayerControl player;
    public int levelNum;
    public int encounterNum;

    // Encounter Vars
    public int numOfGates;
    public float gateSpeed;
    public int cameraIndex;
    public Vector3[] gateTargetPosition;
    public Vector2 winCheckpoint;
    Vector3[] gateOriginPosition; // Place holder for origin position
    bool isGatesMove = false; // Gates can move
    bool isEncounterFinished = false;
    bool isArtefactSpawner = false;

    EnemySpawner enemySpawner;
    GameObject encounterCamera;
    private PlayerHUD playerHUD;

    private void Awake() {
        enemySpawner = GetComponent<EnemySpawner>();
    }

    private void Start() {
        gateOriginPosition = new Vector3[numOfGates];
        gm = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameManager>();
        player = GameObject.FindGameObjectWithTag("ThePlayer").GetComponent<PlayerControl>();
        playerHUD = GameObject.FindGameObjectWithTag("Player HUD").GetComponent<PlayerHUD>();
        isEncounterFinished = gm.getEncounterFinished(levelNum, encounterNum);
    }

    private void FixedUpdate() {
        if(isGatesMove){
            for(int i=0; i<numOfGates; i++){
                moveGate(i);
            }
            if(checkAllGates()){
                // Debug.Log("Masuk 1");
                isGatesMove = false;
                IsGatesFinishedMove = true;
            }
        }
    }

    // Trigger spawn when player collides
    private void OnTriggerEnter2D(Collider2D collision) {
        if (!isEncounterFinished && collision.gameObject.tag == "ThePlayer" && !isSpawned && !GameManager.isStillInEncounter)
        {
            SpawnEntities();
            player.setCanMove(false);
            GameManager.isStillInEncounter = true;
            encounterCamera = spawnedEntities[cameraIndex];
            // Encounter activated, make sure tag is encounter (harusnya udah fiks)
            if (this.tag == "encounter"){
                encounterCamera.SetActive(true);

                // Save origin position
                for(int i=0; i<numOfGates; i++){
                    gateOriginPosition[i] = spawnedEntities[i].transform.position;
                }

                isGatesMove = true;
            }
        }
    }

    // Only Call for Quiz
    public void ForceSpawn(){
        if (!isEncounterFinished && !isSpawned)
        {
            isArtefactSpawner = true;
            SpawnEntities();
            player.setCanMove(false);
            encounterCamera = spawnedEntities[cameraIndex];
            // Encounter activated, make sure tag is encounter (harusnya udah fiks)
            if (this.tag == "encounter"){
                encounterCamera.SetActive(true);

                // Save origin position
                for(int i=0; i<numOfGates; i++){
                    gateOriginPosition[i] = spawnedEntities[i].transform.position;
                }

                isGatesMove = true;
            }
        }
    }

    bool isGatesFinishedMove = false; // Gates finished moving
    public bool IsGatesFinishedMove
    {
        get { return isGatesFinishedMove ; }
        set
        {
            if( value == isGatesFinishedMove )
                return ;
    
            isGatesFinishedMove = value ;

            // Encounter Start, Gates closed
            if( isGatesFinishedMove && isSpawned && !enemySpawner.getIsSpawned() ){

                // Set Origin as Target
                for(int i=0; i<numOfGates; i++){
                    gateTargetPosition[i] = gateOriginPosition[i];
                }
                // Notify to spawn enemies
                enemySpawner.DoSpawnEntities();
                player.setCanMove(true);
                IsGatesFinishedMove = false;
            }

            // Enemies Defeated, Gates opened
            else if (isGatesFinishedMove && enemySpawner.getIsSpawned()){
                player.setCanMove(true);
                destroyEncounterEntities();
            }
        }    
    }

    // ------------------- ENCOUNTER METHODS -----------------------
    public void FinishEncounter(){
        // player.setCanMove(false);
        isGatesMove = true;
        if(encounterCamera != null){
            encounterCamera.SetActive(false); // Kalo pas zoom in keputus, lambatin gate speed nya
        }
        
        GameManager.isStillInEncounter = false;
        if (isArtefactSpawner){
            // Reset Lives and Health
            GameManager.lives = 3;
            PlayerHPController.hp = GameManager.maxHp;
            playerHUD.UpdateLives();
        }

        // Set checkpoint and global bool
        gm.lastCheckpointPos = winCheckpoint;
        gm.setEncounterFinished(levelNum, encounterNum);

        if(GameManager.isTutorial){
            gm.SetWin();
        }
    }

    // Call to return encounter gates
    void destroyEncounterEntities(){
        for(int i=0; i<spawnManager.numberOfPrefabsToCreate; i++){
            Destroy(spawnedEntities[i]);
        }
    }

    // Check all gate position -> True = Udah bener
    bool checkAllGates(){
        for(int i=0; i<numOfGates; i++){
            Vector3 target = gateTargetPosition[i];
            GameObject gate = spawnedEntities[i];
            if(gate.transform.position != target){
                return false;
            }
        }
        return true;
    }

    void moveGate(int idx){
        // Move Gate by speed
        GameObject gate = spawnedEntities[idx];
        Vector3 target = gateTargetPosition[idx];
        var step = gateSpeed * Time.deltaTime;

        // Debug.Log(target);

        gate.transform.position = Vector3.MoveTowards(gate.transform.position, target, step);
    }
}