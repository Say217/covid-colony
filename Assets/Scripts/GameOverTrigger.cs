﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverTrigger : MonoBehaviour
{
    // Game Manager Vars
    private GameManager gm;

    private void Start() {
        gm = GameObject.FindGameObjectWithTag("Game Manager").GetComponent<GameManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        Debug.Log("You fell!");

        if (collision.gameObject.tag == "ThePlayer")
        {
            // Player dies
            if (GameManager.lives > 0) {
                GameManager.lives -= 1;
                GameManager.hasDied = true;
                GameManager.isStillInEncounter = false;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }

            // Game Over. gak bisa elif
            if (GameManager.lives <= 0)
            {
                Debug.Log("Game Over");
                if(SceneManager.GetActiveScene().name == "TutorialScene"){
                    gm.setLastLevel(0);
                }
                if(SceneManager.GetActiveScene().name == "Level1Scene"){
                    gm.setLastLevel(1);
                }
                if(SceneManager.GetActiveScene().name == "VSFlu"){
                    gm.setLastLevel(1);
                }
                SceneManager.LoadScene("GameOver");
            }
        } else if(collision.gameObject.CompareTag("enemy")){
            collision.gameObject.GetComponent<HPController>().getMySpawner().spawnedDestroyed();
            Destroy(collision.gameObject);
        }
    }
}
